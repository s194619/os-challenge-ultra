#C compiler.
CC = gcc

#C compiler flags.
CFLAGS =-Wall -Wextra -O3 -I inc/

#Ending arguments.
EA = -lssl -lcrypto -pthread

#C source files.
SOURCES = $(shell find src/ -name '*.c')

#Output file.
OUTPUT = server

main: $(cfiles)
	$(CC) $(CFLAGS) $(SOURCES) -o $(OUTPUT) $(EA)
clean: $(OUTPUT)
	rm $(OUTPUT)
.PHONY : clean