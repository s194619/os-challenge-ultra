# About
This project is made by Frederik Verdoner Barba (s194619).

# Overview
The goal of this project is to develop a fast server that can reverse SHA256 hashes sent by clients.

The server developed in this project yields 100% correctness. To achieve a server that runs quickly, I have developed and employed several efficient algorithms and concepts from operating systems. 
In the following sections I will show the experiments I have conducted and their results. Finally, I will also briefly discuss possible future improvements and other considerations.

# Project structure
Each experiment can be pulled and started from its respective branch (“experiment 1”, …). Below I have outlined the contents of the most relevant source files in the project:
-	“experiment.c”: main logic for each experiment (the experiments are divided into functions).
-	“arraylist.c”: array list implementation (used in experiment 4)
-	“heap.c”: binary heap implementation (used in experiment 3 and 4)
-	“map.c”: hash map implementation (used primarily in experiment 2, 4 and 5)
-	“messagequeue.c”: improved priority queue implementation (used in experiment 4)
-	“shautil.c”: functions for inversing a SHA256 hash using a single thread (used for the basic server)
-	“tsha.c”: functions for inversing a SHA256 hash using multiple threads (used in experiment 1)
-   "main.c": the entry point of the application.
-   "program.c": the main logic for the final server.
-   "msg.c": contains functions to start the server, accept clients and get requests.

# Experiments
All experiments build on top of the previous ones (with the exception of experiment 5).
My starting point for the tests and experiments is what I will refer to as the basic server. The basic server is a single threaded application that computes requests in the order they are received. The basic server does not cache results or anything else.
While developing the experiments I have had to closely consider the specifications of the computer that runs the software as well as information about the requests of the clients. 
The VM environment used for the challenge consists of 4 CPUs (2GHz), 512 MB RAM, 20 GB storage.
However, the physical hardware of my system that I have used to conduct experiments and collect data only has 2 CPU cores (1.2 GHz).This may lead to differing results compared to other machines (eg. machines with more cores).
To collect data and compare the experiments I use the following settings for the client generator:

```
SERVER=192.168.101.10
PORT=5003
SEED=5041
TOTAL=40
START=1
DIFFICULTY=40000000
REP_PROB_PERCENT=40
DELAY_US=100000
PRIO_LAMBDA=0.4
```


## Experiment 1: Multithreading SHA256
### Idea
If my server is running on a multicore system, then I can speed up the server by having parts of it run in parallel. If there are idle cores it means that there is excess processing power that could be exploited by multithreading the application.
An optimal place to implement multithreading is during the brute force SHA256 reversing algorithm. Firstly because it is a time-consuming step, meaning any optimizations could potensially have a large impact on the performance of the server. Secondly, there are no problematic data dependencies in the for-loop of the brute force algorithm, making parallelization efficient and straight forward to implement.

### Implementation
There are two ways of implementing multithreading in the server. The first approach is to process multiple requests  the same time by creating a new thread for each request. The second approach is to process one request at a time, but to use multiple threads for that request.
The total execution time using either of the methods should be the same, however, using the second approach leads to a reduced average client waiting time compared to the first, which is why I am going it. Thus, I create n threads and split the range of values to be brute forced into n roughly equally large subdivision and assign each thread a unique subdivision. For instance, given the range [1; 8] and 4 threads each thread a, b, c, d could be assigned subdivisions are follows a = [1;2], b = [3,4], c = [5,6], d = [7,8].
The ideal number of threads to use would be the number of cores that the CPU has, since the number of cores sets the limit for how much can run in true parallel.

### Results
Below are the results of the experiment:

![](imgs/experiment1.png)

Looking at the results, something interesting is observed. The multithreaded server yields a slightly worse score than the basic server. Some of the reasons for this difference could be the following:
-	The CPU on the actual computer running the VM only has 2 cores. With a low core count like this, it means that there will be less benefits from multithreading. Additionally, the server is not the only workload on the 2 CPU cores. The machine is also running windows and other applications in the background which further reduces the benefits of multithreading. 
-	Thread creation takes time and resources. If the time used to create a thread outweighs the benefits of running the algorithm multithreaded then performance could drop instead of increase. For smaller requests with small SHA256 ranges, this effect will be even more visible. A thread pool would therefore make for an interesting future experiment.

The reason for the observed result is likely a combination of the two: low multithreading potential from the system running it and continuous thread creation negatively impacting performance. Additionally, the result could also simply be a case of uncertainty.
To get more conclusive results it would be interesting to see how this experiment fares on a different machine with more cores or by using different parameters in the client generator (such as increasing the difficulty). 


### Threads vs processes
I have chosen to use threads rather than processes to run the program in parallel due to threads being more lightweight, faster, and requiring less recourses. Threads typically also take less time for context switching than processes. Additionally, threads take less time to be create. These factors make them better suited in this situation.

## Experiment 2: Caching results of previous requests
### Idea
Since there is a probability that clients will send the exact same request as another client, I can speed up the server by caching previously calculated results. That way, if two or more clients send the same request, I will only have to calculate the result once for the first one. Future clients will not need to calculate the results themselves but can instead find it in the cache, which is much faster than having to brute force.

### Implementation
I have chosen to implement the cache as a hash map. The SHA256-hash is used as key, and the value is the result (i.e. number that gave the SHA256-hash). For the hash function I simply use the h(x) = x % n, where x is first 32 bits of the SHA256-hash and n is the domain size of the map (i.e., the number of buckets). Assuming the incoming SHA256 hash values are uniformly distributed so is my hash function.
I have chosen to use a hash map because of its efficiency. My server mainly uses the functions Insert, Get and Remove. Below are the running time complexities for a hash map as well as some alternative data structures (n is the number of cache entries).

| Data structure | Insert | Get | Remove |
|---|---|---|---|
| Linked list | O(1) | O(n) | O(n) |
| Binary search tree| O(log n) | O(log n) | O(log n) | 
| Hash map | O(1) | O(1) | O(1) |

Compared to these other data structures using a hash map is advantages, especially when the number of cache entries increase, due to its running time complexity. Additionally, hash maps have relatively low overhead.

### Results
Below are the results of the experiment:

![](imgs/experiment2.png)

As expected, the score is significantly better compared to experiment 1 and the basic server (note: this experiment builds on experiment 1 and uses multithreading). Thus, I keep the solution.

## Experiment 3: Processing requests based on priority (priority queue)
### Idea
Since the score is determined by the waiting time and priority of a request, one way to reduce the score is by calculating the request with high priority as early and quickly as possible. One way to do this is by creating a priority queue. That way, when there are multiple pending requests, the order in which they are computed will depend on their priority. The higher the priority, the sooner the request should be processed.

### Implementation
I have implemented the priority queue as a binary max-heap due to its efficiency. Each node in the heap stores a packet. The binary heap uses the priority level of a request as the key which is used comparisons. My sever mainly uses the functions insert, extract-max and increase-key. Below are the running time complexities for a binary heap as well as alternative data structures (n is the number of heap entries).

| Data structure | Insert | Extract-max | Increase-key |
|---|---|---|---|
| Linked list | O(n) | O(1) | O(n) |
| Binary heap | O(log n) | O(log n) | O(log n) |

While a linked list does have a better running time complexity for insert, the other two operations are significantly slower than for a binary heap. This is not a beneficial trade off since I will be using extract-max and increase-key a lot (increase-key is used a lot in the data structure developed in experiment 4).

### Results

Below are the results of the experiment.

![](imgs/experiment3.png)

It is seen that the experiments is benefical as the score has been reduced.

## Experiment 4: Adjusting priority level based on repetition
### Idea
A way to improve the priority queue is by also taking into account requests the are the same. The key used in the heap to order it should be based on both request priority and the number of duplicate requests. For instance, assume all requests have the same priority. If there are two requests that are identical, then they should be naturally processed before requests that don’t have any duplicates (since this will minimize the score). Thus, the key of a heap node associated with a SHA256 hash should be based on both piority level and the number of requests with the given SHA256 hash. More precisely, the key should be the sum of priorities of requests who have the same SHA256 hash.

### Implementation
I use a binary heap that stores a key alongside an array list of requests containing the same hash. The key is the sum of the priorities in the array list. When a new request is fetched and needs to be added to the heap, I call the increase-key function on the node containing its SHA256 hash and add the newly fetched request to the array list. To efficiently call increase-key I use a hash map. This prevents me from having to search the entire heap to find the node with the matching SHA256 hash. The concept is illustrated below:

![](imgs/experiment4-illustration.png)

When a the SHA256 hash in a node has been computed the answer is sent to all clients contained in the nodes client array.

### Results

The results of the experiment can be seen below.

![](imgs/experiment4.png)

The results show a very noticable and definitely not negligable difference. The experiment yields a even better score than the other making it an overall improvement. This experiment is final version of my server (note: experiment 4 also contains experiment 3, 2 and 1).

## Experiment 5: Caching additional reverse SHA256 values
### Idea
Assuming the results of the SHA256 hashes all fall within the same range of numbers [from; to], then it is possible to cache all values in the range and their hashes. Thus, the brute force algorithm would only have to be run one time, when the server starts instead of every time a request is received. The reply to a request can simply be determined by looking in the cache, which is very fast to do. Since the main cost of this experiment only needs to be payed one time when to initialize the cache, this solution gives a greater pay off with more requests.

A limitation to this solution, however, is that it requires a lot of memory. Additionally, the range of the results of the requests should not be too large. For instance, setting the parameter “start” to be dynamic and randomize in the request generator would mean that the range would be [0;2^64] (2^64 is the max value of uint64) which is not feasible to cache or compute. However, setting “start” to a fixed constant value (like "1" which it is in run-client.sh) and difficulty to a something like “4 000 000” would be more realistic to compute. Since the challenge uses a random start value for each request, I have decided to not include this experiment my final submission for the challenge.

### Implementation
I use a hash map to cache the values.

### Results
For this test i have adjusted the client generator parameters slightly so that I do not run out of memory. I use the following:

```
SERVER=192.168.101.10
PORT=5003
SEED=5041
TOTAL=40
START=1
DIFFICULTY=400000
REP_PROB_PERCENT=40
DELAY_US=100000
PRIO_LAMBDA=0.4
```

The results are seen below:

![](imgs/experiment5.png)

The experiment yeilds yet a better score. Despite this, it is not feasible to include it due to its downsides and limitations.

# Possible improvements

Below are some of possible additions that could be made in the future.

## Using a thread pool for multithreading SHA256
Since creating threads is an expensive operation, one way to speed up the program would be by implementing a thread pool. Currently, each time a SHA256 hash request is being computed new threads are created. However, if I instead used a thread pool then I would only need to create the threads once. Subsequent SHA256 computations would reuse the previously created threads.

## Alternatives to using a binary heap in the priority queue
Instead of using a traditional binary heap for the priority queue other alternatives could be a Fibonacci heap or B-heap.

| Data structure | Insert | Extract-max | Increase-key |
|---|---|---|---|
| Binary Heap | O(log n) | O(log n) | O(log n) |
| B-Heap | O(log n) | O(log n) | O(log n) |
| Fibonacci Heap | O(log n) | O(1) | O(1) |

### Fibonacci heap
A Fibonacci heap has a better running time complex complexity than a binary heap. However, a Fibonacci heap typically also has more overhead than a binary heap. Thus, it would be interesting to test in future in which situation which solution would be ideal depending on the parameters of the test generator.

### B-Heap
A B-heap is a type of binary heap. Compared to the traditional implementation it differs by keeping subtrees in a single page. Traditional heaps are laid out in memory such that if a node is at position n then its children will be at positions 2n and 2n+1 (hence the distance of neighbors in the node array grows exponentially). Thus, a vertical traversal of the tree can be highly inefficient due to how memory is organized into pages in virtual memory. In bigger heaps the next node will likely be on another page. This means that there will likely be an increase in the number of page misses, which is costly. A B-heap solves this problem, which can boost performance especially for big heaps because of the decreased number of pages accessed.
