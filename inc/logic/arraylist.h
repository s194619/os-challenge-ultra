#ifndef ARRAYLIST_H
#define ARRAYLIST_H

#include <stdlib.h>
#include "logic/func.h"

typedef struct {
    size_t arr_len;
    size_t size;
    void *(*items);
} ArrayList;


ArrayList *arraylist_new(size_t arr_len);

void arraylist_delete(ArrayList *al);

void arraylist_add(ArrayList *al, void *item);

void *arraylist_remove_last(ArrayList *al);

void *arraylist_get(ArrayList *al, size_t index);

void arraylist_set(ArrayList *al, size_t index, void* item);

int arraylist_find(ArrayList *al, void *item, compare_func cmp);

size_t arraylist_count(ArrayList *al);

void arraylist_clear(ArrayList *al);

#endif