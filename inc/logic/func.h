#ifndef FUNC_H
#define FUNC_H

typedef unsigned long (*hash_func)(unsigned long dom_size, void *key);

typedef int (*compare_func)(void *p1, void *p2);

#endif