#ifndef HEAP_H
#define HEAP_H

#include <logic/arraylist.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct 
{
    size_t position;
    uint32_t key;
    ArrayList *value;
} 
HeapEntry;

typedef struct 
{
    HeapEntry **H;
    size_t arr_len;
    size_t n;
} 
Heap;

Heap *heap_new(size_t init_size);

void heap_delete(Heap *heap);

ArrayList *heap_max(Heap *heap);

HeapEntry *heap_insert(Heap *heap, uint32_t key, ArrayList *value);

ArrayList *heap_extract_max(Heap *heap);

void heap_increase_key(Heap *heap, size_t index, uint32_t new_key);

uint32_t heap_get_key(Heap *heap, size_t index);

ArrayList *heap_get_value(Heap *heap, size_t index);

size_t heap_position_of(HeapEntry *e);

size_t heap_count(Heap *heap);

#endif