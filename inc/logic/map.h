#ifndef MAP_H
#define MAP_H

#include <stdint.h>
#include <stdlib.h>

#define MAP_DOMAIN_SIZE 100000
#define HASH(x) (*(uint32_t*)(x)) % MAP_DOMAIN_SIZE

typedef struct
{
    struct MapNode *nodes[MAP_DOMAIN_SIZE];
}
Map;

union MapValue
{
    uint64_t number;
    void *ptr;
};

struct MapNode
{
    uint8_t *key;
    union MapValue value;
    struct MapNode *next;
};

Map *map_new();

void map_delete(Map *map);

void map_put(Map *map, uint8_t *key, union MapValue value);

int map_get(Map *map, uint8_t *key, union MapValue *out);

int map_remove(Map *map, uint8_t *key, union MapValue *out);

int map_contains(Map *map, uint8_t *key);

#endif