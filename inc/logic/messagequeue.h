#ifndef MESSAGEQUEUE_H
#define MESSAGEQUEUE_H

#include "network/msg.h"
#include "logic/map.h"
#include "logic/heap.h"

typedef struct{
    Heap *heap;
    Map *map;
} MessageQueue;

MessageQueue *messagequeue_new(size_t init_size);

void messagequeue_delete(MessageQueue *mq);

void messagequeue_add(MessageQueue *mq, Packet *packet);

size_t messagequeue_extract_next(MessageQueue *mq, Packet** buffer, size_t buffer_size);

int messagequeue_is_empty(MessageQueue *mq);

#endif