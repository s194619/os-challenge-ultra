#ifndef SHAUTIL_H
#define SHAUTIL_H

#include <stdint.h>

int is_inv_sha256(uint64_t num, unsigned char *hash);

int inv_sha256(uint64_t* result, unsigned char *value, uint64_t from, uint64_t to);

#endif