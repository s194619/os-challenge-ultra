#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>

void t_init();

int t_inv_sha256(uint64_t* result, unsigned char *value, uint64_t from, uint64_t to);

#endif
