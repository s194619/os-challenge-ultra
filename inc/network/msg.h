#ifndef MSG_H
#define MSG_H

#include <stdint.h>

//Data structure for holding information about a packet sent by the client.
typedef struct Packets {
    uint8_t hash[32];
    uint64_t start;
    uint64_t end;
    uint8_t priority;
    int connfd;
} Packet;

void init_server(int port);

int next_client();

int close_server();

void get_packet(Packet *ret);

void send_answer(uint64_t answer, int connfd);

#endif