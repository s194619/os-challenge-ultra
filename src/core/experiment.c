#include <stdio.h>
#include <openssl/sha.h>
#include <string.h>
#include <endian.h>
#include <pthread.h>

#include "logic/program.h"
#include "network/msg.h"
#include "logic/shautil.h"
#include "logic/tsha.h"
#include "logic/messagequeue.h"

/**
 * Contains the functions to launch the different experiments.
 * Used for the collected data in the readme and study
 * performance differences.
 */

/**********************Basic server (no experiments included)**********************/

/**
 * @brief Starts the basic server (no experiments or improvements).
 */
void start_basic(int port)
{
    printf("Starting progam (using port %d)\n", port);
    init_server(port);

    for(;;){
        Packet p;
        get_packet(&p);
        uint64_t result;
        inv_sha256(&result, p.hash, p.start, p.end);
        send_answer(result, p.connfd);
    }

    close_server();
}



/**********************Experiment 1**********************/

/**
 * @brief Starts the server containing experiment 1 (multithreading).
 * 
 * @param port 
 */
void start_ex1(int port)
{
    printf("Starting progam (using port %d)\n", port);
    init_server(port);

    t_init();

    for(;;){
        Packet p;
        get_packet(&p);
        uint64_t result;
        t_inv_sha256(&result, p.hash, p.start, p.end);
        send_answer(result, p.connfd);
    }

    close_server();
}

/**********************Experiment 2 (+previous experiments included)**********************/

/**
 * @brief Starts the server containing experiment 2 (caching).
 * Also contains experiment 1 (multithreading).
 */
void start_ex2(int port)
{
    printf("Starting progam (using port %d)\n", port);
    init_server(port);

    t_init();
    Map *cache = map_new();

    for(;;){
        Packet *p = malloc(sizeof(Packet));
        uint64_t result;
        get_packet(p);
        union MapValue value;
        if (map_get(cache, p->hash, &value))
        {
            result = value.number;
            send_answer(result, p->connfd);
            continue;
        }
        t_inv_sha256(&result, p->hash, p->start, p->end);
        
        value.number = result;
        map_put(cache, p->hash, value);

        send_answer(result, p->connfd);
    }
    
    close_server();
}

/**********************Experiment 3 (+previous experiments included)**********************/

//Variables used in start_ex3.
pthread_mutex_t mutex_ex3;
Heap *heap_ex3;

/**
 * @brief Used to fetch messages (used in start_ex3).
 */
void *message_getter_ex3(void *arg)
{
    while (1)
    {
        Packet *p = malloc(sizeof(Packet));
        get_packet(p);
        ArrayList *a = arraylist_new(1);
        arraylist_add(a, p);
        pthread_mutex_lock(&mutex_ex3);
        heap_insert(heap_ex3, p->priority, a);
        pthread_mutex_unlock(&mutex_ex3);
    }
    
}

/**
 * @brief Starts the server containing experiment 3 (priority queue).
 * Also contains experiment 2 (caching)
 * Also contains experiment 1 (multithreading).
 */
void start_ex3(int port)
{
    init_server(port);

    t_init();
    Map *cache = map_new();
    heap_ex3 = heap_new(2048);

    pthread_mutex_init(&mutex_ex3, NULL);

    pthread_t message_thread;
    pthread_create(&message_thread, NULL, message_getter_ex3, NULL);

    for(;;){
        Packet *p;
        if (heap_count(heap_ex3) == 0)
            continue;
        pthread_mutex_lock(&mutex_ex3);
        if (heap_count(heap_ex3) == 0){
            pthread_mutex_unlock(&mutex_ex3);
            continue;
        }else
        {
            ArrayList *a = heap_extract_max(heap_ex3);
            p = arraylist_remove_last(a);
            arraylist_delete(a);
            pthread_mutex_unlock(&mutex_ex3);
        }
        uint64_t result;
        union MapValue value;
        if (map_get(cache, p->hash, &value))
        {
            result = value.number;
            send_answer(result, p->connfd);
            continue;
        }
        t_inv_sha256(&result, p->hash, p->start, p->end);
        
        value.number = result;
        map_put(cache, p->hash, value);

        send_answer(result, p->connfd);
    }
    
    close_server();
}


/**********************Experiment 4 (+previous experiments included)**********************/

/**
 * @brief Starts the server containing including experiments 1,2,3 and 4 (improved priority queue).
 */
void start_ex4(int port)
{
    start(port);
}


/**
 * @brief Starts the server containing experiment experiment 5 (caching entire range).
 */
void start_ex5(int port)
{
    printf("Starting progam (using port %d)\n", port);
    init_server(port);

    t_init();

    Map *map = map_new();

    for(;;){
        Packet p;
        get_packet(&p);
        if (!map_contains(map, p.hash))
        {
            for (uint64_t i = p.start; i <= p.end; i++)
            {
                unsigned char *md = malloc(sizeof(unsigned char*) * SHA256_DIGEST_LENGTH);
                SHA256((unsigned char*)&i, sizeof(i), md);
                union MapValue v;
                v.number = i;
                map_put(map, md, v);
            }
        }
        uint64_t result;
        union MapValue value;
        map_get(map, p.hash, &value);
        result = value.number;
        send_answer(result, p.connfd);
    }

    close_server();
}