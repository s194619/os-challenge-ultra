#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>

#include "logic/program.h"
#include "core/experiment.h"

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        printf("Invalid number of arguments (port required).\n");
        return 1;
    }

    printf("Starting Execution.\n");

    char *port_str = argv[1];

    //Parsing the string to an int.
    int port = atoi(port_str);

    //Starting the main logic.
    start(port);

    printf("Execution Complete.\n");
    return 0;
}
