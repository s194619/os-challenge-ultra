#include <stdlib.h>
#include <string.h>
#include "logic/arraylist.h"

/**
 * Contains the logic and functions array lists. 
 */

/**
 * @brief Allocates a new array list.
 */
ArrayList *arraylist_new(size_t arr_len)
{
    if (arr_len <= 0)
        arr_len = 1;
    ArrayList *al = malloc(sizeof(ArrayList));
    al->items = malloc(sizeof(void*)*arr_len);
    al->arr_len = arr_len;
    al->size = 0;
    return al;
}

/**
 * @brief Frees the given array list.
 */
void arraylist_delete(ArrayList *al)
{
    free(al->items);
    free(al);
}

/**
 * @brief Doubles the size of the array that stores the entries on the array list.
 */
void arraylist_grow(ArrayList *al)
{
    void *(*g_items) = malloc(sizeof(void*)*al->arr_len*2);
    memcpy(g_items, al->items, sizeof(void*) * al->arr_len);
    free(al->items);
    al->items = g_items;
    al->arr_len = al->arr_len*2;
}

/**
 * @brief Adds an item to the end of the array list.
 */
void arraylist_add(ArrayList *al, void *item)
{
    if (al->size >= al->arr_len)
        arraylist_grow(al);
    al->items[al->size] = item;
    al->size++;
}

/**
 * @brief Removes the last element from the array list.
 * @return The item that was removed.
 */
void *arraylist_remove_last(ArrayList *al)
{
    al->size--;
    return al->items[al->size];
}

/**
 * @brief Returns the element stored at the given index.
 */
void *arraylist_get(ArrayList *al, size_t index)
{
    return al->items[index];
}

/**
 * @brief Searches for the first element that fits the compare function (ie. where cmp == 0).
 * @return The index of the first element found. Returns -1 if none was found.
 */
int arraylist_find(ArrayList *al, void *item, compare_func cmp)
{
    for (unsigned int i = 0; i < al->size; i++)
    {
        if (cmp(al->items[i], item) == 0)
            return i;
    }
    return -1;
}

/**
 * @brief Removes all entries in the array list.
 */
void arraylist_clear(ArrayList *al)
{
    al->size = 0;
}

/**
 * @brief Computes the number of elements contained in the array list.
 */
size_t arraylist_count(ArrayList *al)
{
    return al->size;
}