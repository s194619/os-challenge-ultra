#include <logic/arraylist.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "network/msg.h"
#include "logic/heap.h"

/**
 * Logic for operations on a binary heap.
 * This is used to implement the priority queue
 * for requests.
 * 
 * The heap uses nodes. Each node has an integer that is
 * used as key (ie. for sorting the heap) and an array list
 * that is the value.
 */

// For calculating the parent, left and right of a node.
#define PARENT(x) x/2
#define LEFT(x) 2*x
#define RIGHT(x) 2*x + 1

#define MAX(x, y) x>y?x:y

/**
 * @brief Allocates a new heap.
 */
Heap *heap_new(size_t init_size)
{
    Heap *ret = malloc(sizeof(Heap));
    ret->H = malloc(sizeof(HeapEntry*)*init_size);
    ret->arr_len = init_size;
    ret->n = 0;
    return ret;
}

/**
 * @brief Frees the given heap.
 */
void heap_delete(Heap *heap)
{
    free(heap->H);
    free(heap);
}

/**
 * @brief Swaps the position of two nodes in the heap.
 */
void heap_swap(Heap *heap, size_t x, size_t y)
{
    HeapEntry *temp = heap->H[x];

    heap->H[x] = heap->H[y];
    heap->H[x]->position = x;

    heap->H[y] = temp;
    heap->H[y]->position = y;
}

/**
 * @brief Reads the value of the largest value in the heap.
 */
ArrayList *heap_max(Heap *heap)
{
    return heap->H[1]->value;
}

/**
 * Bubble up and bubble down is used to manage the heap
 * such that the heap invariant is not broken.
 */

void heap_bubble_up(Heap *heap, size_t n)
{
    while(n != 1 &&
        heap->H[PARENT(n)]->key < heap->H[n]->key)
    {
        heap_swap(heap, n, PARENT(n));
        n = PARENT(n);
    }
}

void heap_bubble_down(Heap *heap, size_t n)
{
    while (1)
    {
        size_t child;
        if ((LEFT(n) <= heap->n && RIGHT(n) <= heap->n) &&
        (heap->H[LEFT(n)]->key > heap->H[n]->key || heap->H[RIGHT(n)]->key > heap->H[n]->key))
        {
            child = RIGHT(n);        
        if (heap->H[LEFT(n)]->key > heap->H[RIGHT(n)]->key)
            child = LEFT(n);
        }
        else if (LEFT(n) <= heap->n && heap->H[LEFT(n)]->key > heap->H[n]->key)
        {
            child = LEFT(n);
        }
        else if (RIGHT(n) <= heap->n && heap->H[RIGHT(n)]->key > heap->H[n]->key)
        {
            child = RIGHT(n);
        }
        else
        {
            return;
        }
        heap_swap(heap, n, child);
        n = child;
    }
}

/**
 * @brief Doubles the size of the array used in the heap.
 */
void heap_grow(Heap *heap)
{
    HeapEntry *(*g_items) = malloc(sizeof(HeapEntry*)*heap->arr_len*2);
    memcpy(g_items, heap->H, sizeof(HeapEntry*) * heap->arr_len);
    free(heap->H);
    heap->H = g_items;
    heap->arr_len = heap->arr_len*2;
}

/**
 * @brief Inserts a value into the heap.
 */
HeapEntry *heap_insert(Heap *heap, uint32_t key, ArrayList *value)
{
    if (heap_count(heap) >= heap->arr_len)
        heap_grow(heap);
    heap->n += 1;
    HeapEntry *e = malloc(sizeof(HeapEntry));
    e->key = key;
    e->value = value;
    e->position = heap->n;
    heap->H[heap->n] = e;
    heap_bubble_up(heap, heap->n);
    return e;
}

/**
 * @brief Removess the node with the greatest value.
 * @return The value of the node.
 */
ArrayList *heap_extract_max(Heap *heap)
{
    ArrayList *ret = heap->H[1]->value;
    heap_swap(heap, 1, heap->n);
    free(heap->H[heap->n]);
    heap->n -= 1;
    heap_bubble_down(heap, 1);
    return ret;    
}

/**
 * @brief Updates increases in the key of node the given index in the heap.
 */
void heap_increase_key(Heap *heap, size_t index, uint32_t new_key)
{
    heap->H[index]->key = new_key;
    heap_bubble_up(heap, index);
}

/**
 * @brief Gets the key of the node of a given index.
 */
uint32_t heap_get_key(Heap *heap, size_t index)
{
    return heap->H[index]->key;
}

/**
 * @brief Gets the value of the node of a given index.
 */
ArrayList *heap_get_value(Heap *heap, size_t index)
{
    return heap->H[index]->value;
}

/**
 * @brief Gets the position of the node in the heap.
 */
size_t heap_position_of(HeapEntry *e){
    return e->position;
}

/**
 * @brief Computes the number of entries in the heap.
 */
size_t heap_count(Heap *heap)
{
    return heap->n;
}