#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "logic/map.h"

/**
 * Logic for computing on a hash map.
 * The hash map uses the a SHA256 hash as key. 
 * The value of an entry is either a pointer
 * or an uint64_t (implemented using a union).
 * Using a union as value for the key-value-pair
 * allows the map implementation to
 * be used in multiple contexts.
 * 
 * It is used for caching previous results as 
 * well as enhancements to the request priority
 * queue system.
 */

/**
 * @brief Dynamically allocates and initializes a new hash map.
 */
Map *map_new()
{
    Map *ret = malloc(sizeof(Map));
    return ret;
}

/**
 * @brief Frees the given hash map and its underlying data.
 */
void map_delete(Map *map)
{
    for (size_t i = 0; i < MAP_DOMAIN_SIZE; i++)
    {
        struct MapNode *node = map->nodes[i];
        while(node != NULL){
            struct MapNode *next = node->next;
            free(node);
            node = next;
        }
    }
    free(map);
}

/**
 * @brief Finds the entry associated with the given key.
 * @return The entry if found. Otherwise returns NULL.
 */
struct MapNode *map_get_node(Map *map, uint8_t *key)
{
    struct MapNode *node = map->nodes[HASH(key)];
    while(node != NULL && memcmp(node->key, key, 32) != 0)
            node = node->next;
    return node;
}

/**
 * @brief Adds the key-value-pair to the map.
 */
void map_put(Map *map, uint8_t *key, union MapValue value)
{
    struct MapNode *node = map_get_node(map, key);
    if (node != NULL)
    {
        node->value = value;
        return;
    }
    struct MapNode *new = malloc(sizeof(struct MapNode));
    new->key = key; new->value = value; new->next = map->nodes[HASH(key)];
    map->nodes[HASH(key)] = new;    
}

/**
 * @brief Gets the value of the entry with the given key.
 * @return 1 if entry is found. Else returns 0. 
 */
int map_get(Map *map, uint8_t *key, union MapValue *out)
{
    struct MapNode *node = map_get_node(map, key);
    if (node == NULL)
        return 0;
    *out = node->value;
    return 1;
}

/**
 * @brief Removes an entry from the map with the given key.
 * @return 1 if key was previously contained. 0 otherwise.
 */
int map_remove(Map *map, uint8_t *key, union MapValue *out)
{
    struct MapNode *node = map->nodes[HASH(key)];
    struct MapNode *prev = NULL;
    while (node != NULL && memcmp(node->key, key, 32))
    {
        prev = node;
        node = node->next;
    }
    if (node == NULL)
        return 0;
    if (prev == NULL)
        map->nodes[HASH(key)] = node->next;
    else
        prev->next = node->next;
    
    *out = node->value;
    free(node);
    return 1;
}

/**
 * @brief Checks if the key exists in the map.
 * @return 1 on true. 0 on false.
 */
int map_contains(Map *map, uint8_t *key)
{
    union MapValue x;
    return map_get(map, key, &x);
}
