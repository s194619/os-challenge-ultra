#include <stdio.h>
#include "logic/messagequeue.h"

/**
 * Contains the logic for the advanced priority queue (store in the struct "MessageQueue").
 * Instead of soley looking a the priority level of the
 * requests to determine their placement in the heap, this
 * datastructure also looks at both the priority levels of 
 * the requests, as well as whether there are dupliate requests.
 * If there are many of the same requests they should obtain a higher
 * priority.
 * 
 * Packets are grouped by having a common SHA256 hash in the heap (the heap stores array lists).
 * Additionally, a map points to the position of the groups such that increase-key can be efficiently
 * called when the priority level in the heap must be updated (eg. new packet has been retrieve that shares a key
 * with an existing group, hereby increasing the priority level in the heap).
 */

/**
 * @brief Allocates a new message queue.
 */
MessageQueue *messagequeue_new(size_t init_size)
{
    MessageQueue *ret = malloc(sizeof(MessageQueue));
    ret->heap = heap_new(init_size);
    ret->map = map_new();
    return ret;
}

/**
 * @brief Frees the given message queue.
 */
void messagequeue_delete(MessageQueue *mq)
{
    heap_delete(mq->heap);
    map_delete(mq->map);
    free(mq);
}

/**
 * @brief Returns the next group of requests that should be processed next.
 * The requests will be put into the given buffer. The requests in the buffer
 * will all be requesting the inverse of the same SHA256 hash.
 * @return The number of elements.
 */
size_t messagequeue_extract_next(MessageQueue *mq, Packet** buffer, size_t buffer_size)
{
    ArrayList *al = heap_max(mq->heap);
    //Every arraylist in the heap must contain atleast one packet.
    //If this is not the case then something when wrong.
    //Additionally, the buffer must naturally be greater than zero for
    //it to hold the retrieved packets.
    if (arraylist_count(al) == 0 || buffer_size <= 0)
    {
        printf("Invalid state/args in messagequeue. \n");
        return -1;
    }

    unsigned int i = 0;
    for (; i < buffer_size && arraylist_count(al) > 0; i++)
    {
        buffer[i] = arraylist_remove_last(al);
    }
    //If the arraylist is not empty then all packets for the group 
    //have been fetched and the arraylist can be remove from the heap
    //and the SHA256 hash that the packets shared can be removed from the map.
    if (arraylist_count(al) == 0)
    {
        heap_extract_max(mq->heap);
        union MapValue out;
        map_remove(mq->map, buffer[0]->hash, &out);
        free(al);
    }
    return i;
}

/**
 * @brief Adds a request to the message queue.
 */
void messagequeue_add(MessageQueue *mq, Packet *packet)
{
    union MapValue index;
    //If the item is not in the map already then add a new entry to the map and heap.
    //If it is in the map, then it is also in the heap.
    if (!map_get(mq->map, packet->hash, &index))
    {
        //Adding a new array list to the heap that stores packets with this SHA256 hash.
        ArrayList *al = arraylist_new(4);
        index.ptr = &heap_insert(mq->heap, 0, al)->position;
        map_put(mq->map, packet->hash, index);
    }
    //Add the packet to its proper array list and update the key (since now its priority got greater).
    size_t *position_ptr = index.ptr;
    arraylist_add(heap_get_value(mq->heap, *position_ptr), packet);
    heap_increase_key(mq->heap, *position_ptr, heap_get_key(mq->heap, *position_ptr) + packet->priority);
}


int messagequeue_is_empty(MessageQueue *mq)
{
    return heap_count(mq->heap) == 0;
}