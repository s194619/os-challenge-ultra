#include <stdio.h>
#include <openssl/sha.h>
#include <string.h>
#include <endian.h>
#include <pthread.h>

#include "logic/program.h"
#include "network/msg.h"
#include "logic/shautil.h"
#include "logic/tsha.h"
#include "logic/messagequeue.h"

#define MESSAGE_GROUP_BUFFER_SIZE 2000

MessageQueue *messagequeue;
pthread_mutex_t mq_mutex;
pthread_mutex_t c_mutex;
Map *cache;

/**
 * @brief For getting the messages and putting them into the upgraded priority queue.
 */
void *get_messages(void* arg)
{
    while (1)
    {
        Packet *p = malloc(sizeof(Packet));
        get_packet(p);
        union MapValue val;
        int exists = 0;

        pthread_mutex_lock(&c_mutex);
        exists = map_get(cache, p->hash, &val);
        pthread_mutex_unlock(&c_mutex);

        if (exists)
        {
            //Value already exists in cache.
            //Thus no need to at it the queue of requests to compute.
            //Instead just reply immidiately.
            send_answer(val.number, p->connfd);
            free(p);
        }
        else
        {
        //Message is not in the cache.
        //Thus add it to the queue.
        pthread_mutex_lock(&mq_mutex);
        messagequeue_add(messagequeue, p);
        pthread_mutex_unlock(&mq_mutex);
        }
    }
}

/**
 * @brief Starts the program for running the server.
 */
void start(int port)
{
    printf("Starting progam (port: %d)\n", port);
    init_server(port);

    messagequeue = messagequeue_new(2048);
    cache = map_new();

    pthread_mutex_init(&mq_mutex, NULL);
    pthread_mutex_init(&c_mutex, NULL);

    pthread_t message_thread;
    pthread_create(&message_thread, NULL, get_messages, NULL);
    
    Packet *buffer[MESSAGE_GROUP_BUFFER_SIZE];

    while(1)
    {
        if (!messagequeue_is_empty(messagequeue)){
            pthread_mutex_lock(&mq_mutex);
            if (!messagequeue_is_empty(messagequeue))
            {
                //fetching the next group of packets to process.
                size_t size = messagequeue_extract_next(messagequeue, buffer, MESSAGE_GROUP_BUFFER_SIZE);
                pthread_mutex_unlock(&mq_mutex);
                
                uint64_t result;
                Packet *rep = buffer[0];
                
                //Calculating the inverse of the SHA256 hash.
                //The inverse only has to be computed oncce
                //since all packets in the group have the same SHA256 hash.
                t_inv_sha256(&result, rep->hash, rep->start, rep->end);

                //The answer computed is sent to all the clients.
                for (unsigned int i = 0; i < size; i++)
                {
                    Packet *p = buffer[i];
                    send_answer(result, p->connfd);
                    //currently does not free packets because some are needed to be stored in the cache.
                }

                //Putting the value into the cache.
                union MapValue val;
                val.number = result;
                pthread_mutex_lock(&c_mutex);
                map_put(cache, rep->hash, val);
                pthread_mutex_unlock(&c_mutex);
            }
            else
            {
                pthread_mutex_unlock(&mq_mutex);
            }
        
        }
    }

}