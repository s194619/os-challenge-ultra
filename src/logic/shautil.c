#include "logic/shautil.h"
#include <openssl/sha.h>
#include <string.h>

/**
 * Contains functions and utilities for computing 
 * the inverse of SHA256.
 */


/**
 * @brief Checks if a number computes the given SHA256 hash.
 * @return 1 for true. 0 for false. 
 */
int is_inv_sha256(uint64_t num, unsigned char *hash)
{
    //Computes SHA256 for i.
    unsigned char md[SHA256_DIGEST_LENGTH];
    SHA256((unsigned char*)&num, sizeof(num), md);
    //Check if the result of SHA256 is the result being looked for.
    return memcmp(hash, md, SHA256_DIGEST_LENGTH) == 0;
}

/**
 * @brief Single threaded brute force computation of the inverse SHA256 of the number in the range [from; to] 
 * @return 1 if a result is found in the range. 0 if no result is found.
 */
int inv_sha256(uint64_t* result, unsigned char *value, uint64_t from, uint64_t to)
{
    for (uint64_t i = from; i <= to; i++){
        if (is_inv_sha256(i, value))
        {
            //The inverse SHA256 is found.
            *result = i;
            return 1;
        }
    }
    //The inverse SHA256 is not in [from; to].
    return 0;
}