#include "logic/shautil.h"
#include <openssl/sha.h>
#include <pthread.h>
#include <semaphore.h>

#include <stdio.h>

/**
 * Contains functions for computing the inverse of SHA256
 * using multiple threads.
 */


// The number of threads used to compute the inverse.
#define THREAD_COUNT 4

pthread_mutex_t mutex;

/**
 * Contains the arguments that are passed to the
 * threads to compute the invsese SHA256.
 */
typedef struct{
    uint64_t* result;
    unsigned char *value;
    uint64_t from;
    uint64_t to;
} tsha_inv_t;

/**
 * @brief Sets up for the use of multithreaded inverse SHA256.
 */
void t_init()
{
    pthread_mutex_init(&mutex, NULL);
}

/**
 * @brief Used for the different threads to calculate the inverse of a SHA256 hash.
 */
void *t_inv_sha256_routine(void *arg)
{
    tsha_inv_t *inf = (tsha_inv_t*) arg;
    for (uint64_t i = inf->from; i <= inf->to; i++){
        if (is_inv_sha256(i, inf->value))
        {
            // The inverse SHA256 is found.

            // Unlocks the mutex such that the coorditing
            // thread can continue.
            pthread_mutex_unlock(&mutex);
            *(inf->result) = i;

            return NULL;
        }
    }
    // Result was not found in the given range.
    return NULL;
}

/**
 * @brief Computes the inverse of a SHA256 hash using multiple threads.
 * @return 1 if a result is found. 0 if no result is found.
 */
int t_inv_sha256(uint64_t* result, unsigned char *value, uint64_t from, uint64_t to)
{
    pthread_t threads[THREAD_COUNT];
    tsha_inv_t infs[THREAD_COUNT];

    uint64_t dom = to - from;

    //locks the mutex indicating that a result is being calculated.
    pthread_mutex_lock(&mutex);

    //Creating the threads.
    for (int i = 0; i < THREAD_COUNT; i++)
    {
        tsha_inv_t *inf = &infs[i];
        inf->result = result;
        inf->value = value;
        // Calculating the subdivision of the full range
        // That each thread will compute on.
        inf->from = dom*i/THREAD_COUNT + from;
        inf->to = dom*(i + 1)/THREAD_COUNT + from;

        //Starts the thread.
        pthread_create(&threads[i], NULL, t_inv_sha256_routine, inf);
    }

    // Waits until a result is found.
    // When a result is found a thread will release the mutex.
    pthread_mutex_lock(&mutex);

    // Now a result has been found meaning that 
    // execution on the other threads can be stopped.

    for (int i = 0; i < THREAD_COUNT; i++)
    {
        pthread_cancel(threads[i]);
    }
    
    for (int i = 0; i < THREAD_COUNT; i++)
    {
        pthread_join(threads[i], NULL);
    }

    pthread_mutex_unlock(&mutex);
    
    return 1;
}
