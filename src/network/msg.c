#include <stdio.h>
#include <stdlib.h>

#include <netdb.h>
#include <netinet/in.h>

#include <string.h>

#include <unistd.h>

#include "network/messages.h"
#include "network/msg.h"

int sockfd = -1;
char buffer[PACKET_REQUEST_SIZE];

/**
 * @brief Initializes the server, so that it is ready to accept clients.
 * 
 */
void init_server(int port)
{
    printf("Initializing server...\n");

    //Creating the socket.
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in server_addr;
    if (sockfd < 0){
        printf("Error creating socket.\n");
        _exit(-1);
    }

    bzero((char *) (&server_addr), sizeof(server_addr));
    
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(port);

    //Binding.
    int bind_status = bind(sockfd, (struct sockaddr *)(&server_addr), sizeof(server_addr));
    if (bind_status < 0){
        printf("Error on binding.\n");
        _exit(-1);
    }

    listen(sockfd, 100);

    printf("Server initialized sucessfully.\n");
}

/**
 * @brief Closes the server.
 */
int close_server()
{
    printf("Closing server.\n");
    return shutdown(sockfd, 2);
}

/**
 * @brief Interprets and stores the data in the buffer in the packet.
 */
void buf_to_packet(char buf[PACKET_REQUEST_SIZE], Packet *ret)
{
    memcpy(&(ret->hash), buf, 32);
    memcpy(&(ret->start), buf + 32, 8);
    memcpy(&(ret->end), buf + 40, 8);
    memcpy(&(ret->priority), buf + 48, 1);

    //Endian conversion.
    ret->start = be64toh(ret->start);
    ret->end = be64toh(ret->end);
}

/**
 * @brief Accepts the next client and reads the packet sent.
 */
void get_packet(Packet *ret)
{
    //Accepting the client:

    struct sockaddr_in client_addr;

    unsigned int client_addr_len = sizeof(client_addr);
    int connfd = accept(sockfd, (struct sockaddr *) (&client_addr), &client_addr_len);

    if (connfd < 0)
    {
        printf("Error on accept.\n");
        _exit(-1);
    }

    //Reading the data sent:

    int size = read(connfd, buffer, PACKET_REQUEST_SIZE);

    if (size < 0){
        printf("Error reading from socket.\n");
        _exit(-1);
    }

    buf_to_packet(buffer, ret);
    ret->connfd = connfd;
}

/**
 * @brief Sends the answer to the client.
 */
void send_answer(uint64_t answer, int connfd)
{
    //Endian conversion.
    answer = htobe64(answer);
    if (write(connfd, &answer, 8) < 0)
        printf("Error sending message. \n");
    shutdown(connfd, 2);
}